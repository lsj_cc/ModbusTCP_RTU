# Modbus优化方案书

## 需求分析

多线程

越界访问

发送端（客户端）

1. 上传文件
2. 同步文件列表
3. 下载文件

接收端（服务器）

1. 传输文件
2. 添加文件夹
3. 删除条目

## 发送端

### 1.整体流程

![](Picture/发送端整体流程.png)



### 2.上传文件

客户端通过点击上传文件按钮，绑定对应的槽函数，文件流打开本地文件，启动文件传输，从套接字存入文件总大小命令、文件名信息，开始传输文件数据，等待客户端传输完毕，服务器接收文件。

![](Picture/上传文件.png)





### 3.同步文件列表

客户端通过点击按钮，同步文件槽函数绑定，设置服务器和客户端的Qt版本，客户端保留总大小信息空间、文件名信息空间，并填充实际的存储空间，如果列表不为空，使能下载按钮，同时将服务器的文件列表显示到客户端界面。

![](Picture/同步文件列表.png)



### 4.下载文件

客户端通过点击下载文件按钮，将下载文件参函数绑定，获得要下载的文件名字和文件信息大小，从服务器同步数据信息，如果剩余数据大小不为0，则继续传输，直至数据传输完毕，同时在客户端本地创建文件。

![](Picture/下载文件.png)



## 接收端

### 1.整体流程

服务器将子线程分离出来，同时初始化变量，客户端上传文件，同时获取文件名信息大小，服务器下载到本地文件。析构释放内存。

![](Picture/接收端整体流程.png)



### 2.传输文件

服务器通过点击上传文件按钮，绑定对应的槽函数，文件流打开本地文件，启动文件传输，从套接字存入文件总大小命令、文件名信息，开始传输文件数据，等待服务器传输完毕，客户端接收文件。

![](Picture/服务器传输文件.png)



### 3.添加文件夹

服务器通过点击添加文件夹按钮，将添加文件夹槽函数绑定，以列表形式存放文件名信息。

![](Picture/文件夹添加.png)

### 4.删除条目

![](Picture/删除列表条目.png)

## 越界访问

通常 modbus 协议的保持寄存器地址范围在 40001 – 49999 之间，对于多数应用来说已经够了

### 方案一：

modbus Master 协议库支持超过 9999 的保持寄存器地址。地址范围为 400001 – 465536。只需在调用 MBUS_MSG 子程序时给 Addr 参数赋相应的值即可，如 416768。

Modubs Master 扩展地址模式仅支持保持寄存器区，不支持其他地址类型。

![](Picture/越界访问.png)

### 方案二：

用自定义的公共功能码：

![](Picture/越界访问1.png)



