#ifndef DIALOG0F_H
#define DIALOG0F_H

#include <QDialog>
#include <QLineEdit>


namespace Ui {
class Dialog0f;
}

class Dialog0f : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog0f(QWidget *parent = nullptr);
    ~Dialog0f();
    //获取输入信息
     QVector<quint16> infdata();
     //获取用户信息
     QByteArray getUserInputData();
private slots:
     void on_over_clicked();

     void on_start_btn_clicked();
     void on_tablecoil_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn);
     void on_tablecoil_cellDoubleClicked(int row, int column);
private:
    Ui::Dialog0f *ui;
    QVector<quint16> inf;
    //线圈表格初始化
    quint8 initdata[1968]={0};
    QByteArray bamsg;
    QRegExp rx;
    QString pre;
    int isfc=0;
    int isclicked=0;
    QLineEdit* edit;
    quint16 oldstartaddr;
    void inittable();
    void tabledatainit(quint16 BeginAddress, quint16 Number);

    void GetData0X01(QByteArray &coilsDataArr, quint16 BeginAddress, quint16 Number);
};

#endif // DIALOG0F_H
