/********************************************************************************
** Form generated from reading UI file 'widget.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGET_H
#define UI_WIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Widget
{
public:
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_3;
    QLineEdit *ClientLineEdit;
    QPushButton *ClientConnectPB;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QHBoxLayout *horizontalLayout_4;
    QPushButton *SynFilePtn;
    QPushButton *DownloadPB;
    QTextEdit *textEdit;
    QListWidget *listWidget;

    void setupUi(QWidget *Widget)
    {
        if (Widget->objectName().isEmpty())
            Widget->setObjectName(QString::fromUtf8("Widget"));
        Widget->resize(401, 483);
        gridLayout = new QGridLayout(Widget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        ClientLineEdit = new QLineEdit(Widget);
        ClientLineEdit->setObjectName(QString::fromUtf8("ClientLineEdit"));

        horizontalLayout_3->addWidget(ClientLineEdit);

        ClientConnectPB = new QPushButton(Widget);
        ClientConnectPB->setObjectName(QString::fromUtf8("ClientConnectPB"));

        horizontalLayout_3->addWidget(ClientConnectPB);


        verticalLayout_2->addLayout(horizontalLayout_3);

        pushButton = new QPushButton(Widget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        verticalLayout_2->addWidget(pushButton);

        pushButton_2 = new QPushButton(Widget);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));

        verticalLayout_2->addWidget(pushButton_2);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        SynFilePtn = new QPushButton(Widget);
        SynFilePtn->setObjectName(QString::fromUtf8("SynFilePtn"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(SynFilePtn->sizePolicy().hasHeightForWidth());
        SynFilePtn->setSizePolicy(sizePolicy);

        horizontalLayout_4->addWidget(SynFilePtn);

        DownloadPB = new QPushButton(Widget);
        DownloadPB->setObjectName(QString::fromUtf8("DownloadPB"));
        sizePolicy.setHeightForWidth(DownloadPB->sizePolicy().hasHeightForWidth());
        DownloadPB->setSizePolicy(sizePolicy);

        horizontalLayout_4->addWidget(DownloadPB);


        verticalLayout_2->addLayout(horizontalLayout_4);


        verticalLayout->addLayout(verticalLayout_2);


        gridLayout->addLayout(verticalLayout, 0, 0, 1, 1);

        textEdit = new QTextEdit(Widget);
        textEdit->setObjectName(QString::fromUtf8("textEdit"));

        gridLayout->addWidget(textEdit, 2, 0, 1, 1);

        listWidget = new QListWidget(Widget);
        listWidget->setObjectName(QString::fromUtf8("listWidget"));

        gridLayout->addWidget(listWidget, 3, 0, 1, 1);


        retranslateUi(Widget);

        QMetaObject::connectSlotsByName(Widget);
    } // setupUi

    void retranslateUi(QWidget *Widget)
    {
        Widget->setWindowTitle(QCoreApplication::translate("Widget", "Form", nullptr));
        ClientLineEdit->setText(QCoreApplication::translate("Widget", "10.100.14.37", nullptr));
        ClientConnectPB->setText(QCoreApplication::translate("Widget", "\350\277\236\346\216\245/\346\226\255\345\274\200", nullptr));
        pushButton->setText(QCoreApplication::translate("Widget", "\344\270\212\344\274\240\346\226\207\344\273\266", nullptr));
        pushButton_2->setText(QCoreApplication::translate("Widget", "\346\270\205\345\261\217", nullptr));
        SynFilePtn->setText(QCoreApplication::translate("Widget", "\345\220\214\346\255\245\346\226\207\344\273\266\345\210\227\350\241\250", nullptr));
        DownloadPB->setText(QCoreApplication::translate("Widget", "\344\270\213\350\275\275", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Widget: public Ui_Widget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGET_H
