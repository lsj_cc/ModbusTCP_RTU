#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <qlineedit.h>
#include<QDebug>


namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = nullptr);
    ~Dialog();

    QByteArray getUserInputData();
    QVector<quint16> infdata();

private slots:

   void inittable();
   void tabledatainit(quint16 BeginAddress,quint16 Number);

   void on_tablecoil_cellDoubleClicked(int row, int column);

   void on_tablecoil_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn);

   void on_start_btn_clicked();

   void on_over_clicked();


private:
    Ui::Dialog *ui;
    QVector<quint16> inf;
    quint16 oldstartaddr;
    QByteArray bamsg;

    quint8 initdata[1968]={0};
    int isfc=0;
    int isclicked=0;
    QString pre;
    QLineEdit* edit;
    void GetData0X01(QByteArray &coilsDataArr,quint16 BeginAddress,quint16 Number);
    void byteReverse(QString &coils);
};

#endif // DIALOG_H
