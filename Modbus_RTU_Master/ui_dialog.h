/********************************************************************************
** Form generated from reading UI file 'dialog.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOG_H
#define UI_DIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Dialog
{
public:
    QGridLayout *gridLayout_4;
    QLabel *label_4;
    QWidget *widget_2;
    QGridLayout *gridLayout_3;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QLabel *label;
    QLineEdit *startaddrline;
    QLabel *label_2;
    QLineEdit *addrline;
    QLineEdit *numline;
    QLabel *label_3;
    QPushButton *over;
    QPushButton *start_btn;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_2;
    QTableWidget *tablecoil;

    void setupUi(QDialog *Dialog)
    {
        if (Dialog->objectName().isEmpty())
            Dialog->setObjectName(QString::fromUtf8("Dialog"));
        Dialog->resize(404, 420);
        gridLayout_4 = new QGridLayout(Dialog);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        label_4 = new QLabel(Dialog);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setMinimumSize(QSize(0, 30));

        gridLayout_4->addWidget(label_4, 0, 0, 1, 1);

        widget_2 = new QWidget(Dialog);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        gridLayout_3 = new QGridLayout(widget_2);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        groupBox = new QGroupBox(widget_2);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        startaddrline = new QLineEdit(groupBox);
        startaddrline->setObjectName(QString::fromUtf8("startaddrline"));

        gridLayout->addWidget(startaddrline, 1, 2, 1, 1);

        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        addrline = new QLineEdit(groupBox);
        addrline->setObjectName(QString::fromUtf8("addrline"));

        gridLayout->addWidget(addrline, 0, 2, 1, 1);

        numline = new QLineEdit(groupBox);
        numline->setObjectName(QString::fromUtf8("numline"));

        gridLayout->addWidget(numline, 2, 2, 1, 1);

        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout->addWidget(label_3, 2, 0, 1, 1);

        over = new QPushButton(groupBox);
        over->setObjectName(QString::fromUtf8("over"));

        gridLayout->addWidget(over, 3, 2, 1, 1);

        start_btn = new QPushButton(groupBox);
        start_btn->setObjectName(QString::fromUtf8("start_btn"));

        gridLayout->addWidget(start_btn, 3, 0, 1, 1);


        gridLayout_3->addWidget(groupBox, 0, 0, 1, 1);

        groupBox_2 = new QGroupBox(widget_2);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        gridLayout_2 = new QGridLayout(groupBox_2);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        tablecoil = new QTableWidget(groupBox_2);
        if (tablecoil->columnCount() < 2)
            tablecoil->setColumnCount(2);
        if (tablecoil->rowCount() < 1968)
            tablecoil->setRowCount(1968);
        tablecoil->setObjectName(QString::fromUtf8("tablecoil"));
        tablecoil->setRowCount(1968);
        tablecoil->setColumnCount(2);

        gridLayout_2->addWidget(tablecoil, 0, 0, 1, 1);


        gridLayout_3->addWidget(groupBox_2, 1, 0, 1, 1);


        gridLayout_4->addWidget(widget_2, 1, 0, 1, 1);


        retranslateUi(Dialog);

        QMetaObject::connectSlotsByName(Dialog);
    } // setupUi

    void retranslateUi(QDialog *Dialog)
    {
        Dialog->setWindowTitle(QCoreApplication::translate("Dialog", "Dialog", nullptr));
        label_4->setText(QCoreApplication::translate("Dialog", "\345\212\237\350\203\275\347\240\2010F\350\256\276\347\275\256", nullptr));
        groupBox->setTitle(QCoreApplication::translate("Dialog", "\350\276\223\345\205\245\357\274\232", nullptr));
        label->setText(QCoreApplication::translate("Dialog", "\344\273\216\346\234\272\345\234\260\345\235\200\357\274\232", nullptr));
        startaddrline->setPlaceholderText(QCoreApplication::translate("Dialog", "\350\276\223\345\205\245\350\265\267\345\247\213\345\234\260\345\235\200(0-65535)", nullptr));
        label_2->setText(QCoreApplication::translate("Dialog", "\350\265\267\345\247\213\345\234\260\345\235\200\357\274\232", nullptr));
        addrline->setPlaceholderText(QCoreApplication::translate("Dialog", "\350\276\223\345\205\245\344\273\216\346\234\272\345\234\260\345\235\200(1-247)", nullptr));
        numline->setPlaceholderText(QCoreApplication::translate("Dialog", "\350\276\223\345\205\245\346\225\260\351\207\217(1-1968)", nullptr));
        label_3->setText(QCoreApplication::translate("Dialog", "\347\272\277\345\234\210\346\225\260\351\207\217\357\274\232", nullptr));
        over->setText(QCoreApplication::translate("Dialog", "\345\256\214\346\210\220", nullptr));
        start_btn->setText(QCoreApplication::translate("Dialog", "\350\256\276\347\275\256", nullptr));
        groupBox_2->setTitle(QCoreApplication::translate("Dialog", "\347\272\277\345\234\210\350\256\276\345\200\274\357\274\232", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Dialog: public Ui_Dialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOG_H
