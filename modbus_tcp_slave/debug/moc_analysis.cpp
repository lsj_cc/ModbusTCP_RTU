/****************************************************************************
** Meta object code from reading C++ file 'analysis.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.14.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../analysis.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QVector>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'analysis.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.14.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_analysis_t {
    QByteArrayData data[15];
    char stringdata0[133];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_analysis_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_analysis_t qt_meta_stringdata_analysis = {
    {
QT_MOC_LITERAL(0, 0, 8), // "analysis"
QT_MOC_LITERAL(1, 9, 11), // "toUishowMsg"
QT_MOC_LITERAL(2, 21, 0), // ""
QT_MOC_LITERAL(3, 22, 8), // "errorMsg"
QT_MOC_LITERAL(4, 31, 15), // "toUishowMsgPack"
QT_MOC_LITERAL(5, 47, 7), // "packMsg"
QT_MOC_LITERAL(6, 55, 13), // "analysis_over"
QT_MOC_LITERAL(7, 69, 2), // "ba"
QT_MOC_LITERAL(8, 72, 10), // "wirtTabler"
QT_MOC_LITERAL(9, 83, 3), // "num"
QT_MOC_LITERAL(10, 87, 9), // "satrtaddr"
QT_MOC_LITERAL(11, 97, 16), // "QVector<quint16>"
QT_MOC_LITERAL(12, 114, 3), // "bar"
QT_MOC_LITERAL(13, 118, 10), // "wirtTablec"
QT_MOC_LITERAL(14, 129, 3) // "bac"

    },
    "analysis\0toUishowMsg\0\0errorMsg\0"
    "toUishowMsgPack\0packMsg\0analysis_over\0"
    "ba\0wirtTabler\0num\0satrtaddr\0"
    "QVector<quint16>\0bar\0wirtTablec\0bac"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_analysis[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   39,    2, 0x06 /* Public */,
       4,    1,   42,    2, 0x06 /* Public */,
       6,    1,   45,    2, 0x06 /* Public */,
       8,    3,   48,    2, 0x06 /* Public */,
      13,    3,   55,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::QByteArray,    5,
    QMetaType::Void, QMetaType::QByteArray,    7,
    QMetaType::Void, QMetaType::UShort, QMetaType::UShort, 0x80000000 | 11,    9,   10,   12,
    QMetaType::Void, QMetaType::UShort, QMetaType::UShort, QMetaType::QString,    9,   10,   14,

       0        // eod
};

void analysis::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<analysis *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->toUishowMsg((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->toUishowMsgPack((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 2: _t->analysis_over((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 3: _t->wirtTabler((*reinterpret_cast< quint16(*)>(_a[1])),(*reinterpret_cast< quint16(*)>(_a[2])),(*reinterpret_cast< QVector<quint16>(*)>(_a[3]))); break;
        case 4: _t->wirtTablec((*reinterpret_cast< quint16(*)>(_a[1])),(*reinterpret_cast< quint16(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 3:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 2:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<quint16> >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (analysis::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&analysis::toUishowMsg)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (analysis::*)(QByteArray );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&analysis::toUishowMsgPack)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (analysis::*)(QByteArray );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&analysis::analysis_over)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (analysis::*)(quint16 , quint16 , QVector<quint16> );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&analysis::wirtTabler)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (analysis::*)(quint16 , quint16 , QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&analysis::wirtTablec)) {
                *result = 4;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject analysis::staticMetaObject = { {
    QMetaObject::SuperData::link<QObject::staticMetaObject>(),
    qt_meta_stringdata_analysis.data,
    qt_meta_data_analysis,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *analysis::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *analysis::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_analysis.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int analysis::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void analysis::toUishowMsg(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void analysis::toUishowMsgPack(QByteArray _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void analysis::analysis_over(QByteArray _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void analysis::wirtTabler(quint16 _t1, quint16 _t2, QVector<quint16> _t3)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t2))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t3))) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void analysis::wirtTablec(quint16 _t1, quint16 _t2, QString _t3)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t2))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t3))) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
