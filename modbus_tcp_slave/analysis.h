#ifndef ANALYSIS_H
#define ANALYSIS_H

#include <QObject>
#include <QDebug>
#include <qsettings.h>


typedef enum TCP_MODBUS_STATE
{
    //功能码错误
   MB_SLAVE_STATE_FUNCTION_ERROR = 1,
    //数据值错误
   MB_SLAVE_STATE_DATA_ERROR = 2,
    //数据地址错误
   MB_SLAVE_STATE_DATAADDR_ERROR = 3,
    //功能码正确
   MB_SLAVE_STATE_FUNCTION_TRUE = 4,
    //正确帧
   MB_SLAVE_STATE_PACKET_PROCESS = 5,
    //其他
   MB_SLAVE_STATE_PACKET_OTHER = 6,
    //字节与数据字段不正确
   MB_SLAVE_STATE_PACKET_SIZELENTH = 7,

}MB_satae;


class analysis: public QObject
{
    Q_OBJECT
public:
    analysis();
    //接收报文并处理
    void receiveModbus(QByteArray msg, quint8 addr);


signals:
        void toUishowMsg(QString errorMsg);
        void toUishowMsgPack(QByteArray packMsg);
        void analysis_over(QByteArray ba);
        void wirtTabler(quint16 num, quint16 satrtaddr,QVector<quint16> bar);
        void wirtTablec(quint16 num, quint16 satrtaddr,QString bac);


private:
 //--------------变量区-------------------
    quint8  mb_addr;
    QByteArray recvModbusmsg;
    quint16 Copy_protocol;
    quint16 mb_startaddr,mb_num;
    quint8 coilsize;
    QByteArray csizes;
    QByteArray sendModbusmsg;
    QSettings* readseting=nullptr;
     quint16 mb_code;
     quint16 wnum;
     QVector<quint16> bar;
     QVector<quint16> msg;
     QString bac;
     QString errorMsg;


 //--------------函数区--------------------
    MB_satae parse_Modbus_Msg(QByteArray ba);
    void parseModbusMbSatae(MB_satae satae);
    //功能码01解析
    void analysis01();
    void GetData0X01(QByteArray &coilsDataArr, quint16 BeginAddress, quint16 Number);
    void ReadCoilPackMsgToShow(quint16 startaddr, quint16 num, QByteArray msg);

    void analysis10();
    void analysis0f();
    void analysis03();
    void parse_Modbus_Exception_Handling01();
    void parse_Modbus_Exception_Handling02();
    void parse_Modbus_Exception_Handling03();
    void WirteRegsPackMsgToShow(quint16 startaddr, quint16 num, QVector<quint16> msg);
    void WirteCoilPackMsgToShow(quint16 startaddr, quint16 num, QByteArray msg);
    void ReadRegsPackMsgToShow(quint16 startaddr, quint16 num, QVector<quint16> msg);
    void WriteData0X0F(quint16 satrt, QString CoilData);
};

#endif // ANALYSIS_H
