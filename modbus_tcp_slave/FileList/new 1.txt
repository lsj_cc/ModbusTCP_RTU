1.LRC纵向冗余校验
2.tcp的校验
3.帧结构
4.为什么不是多主多多从的
5.广播模式和单播模式


1.* 校验内容不包括起始符和结束符，对报文中除起始符和结束符外的所有连续的8位字节相加，
忽略任何进位，然后求其二进制补码得到LRC，LRC的结果也被编码为2个ASCII码字符。

LRC域是一个包含一个8位二进制值的字节。LRC值由传输设备来计算并放到消息帧中，接收设备在接收消息的过程中计算LRC，
并将它和接收到消息中LRC域中的值比较，如果两值不等，说明有错误。LRC校验比较简单，
它在ASCII协议中使用，检测了消息域中除开始的冒号及结束的回车换行号外的内容。
它仅仅是把每一个需要传输的数据按字节叠加后取反加1即可。

CRC域是两个字节，包含一16位的二进制值。它由传输设备计算后加入到消息中。接收设备重新计算收到消息的CRC，
并与接收到的CRC域中的值比较，如果两值不同，则有误。CRC是先调入一值是全“1”的16位寄存器，
然后调用一过程将消息中连续的8位字节各当前寄存器中的值进行处理。
仅每个字符中的8Bit数据对CRC有效，起始位和停止位以及奇偶校验位均无效。


2.TCP校验和是一个端到端的校验和，由发送端计算，然后由接收端验证。其目的是为了发现TCP首部和数据在发送端到

接收端之间发生的任何改动。如果接收方检测到校验和有差错，则TCP段会被直接丢弃。

TCP校验和覆盖TCP首部和TCP数据，而IP首部中的校验和只覆盖IP的首部，不覆盖IP数据报中的任何数据。

TCP的校验和是必需的，而UDP的校验和是可选的。

3. RTU最大帧长度为256个字节
   TCP最大帧长度为260个字节
   
   modbus rtu
------------------------------ADU-------------------------------------------- 256字节
从站地址(1字节) 功能码(1字节) 数据(0-252字节) CRC(2字节)
PDU=功能码(1字节) 数据(0-252字节)

modbus tcp
=ADU===== 260字节
00 00 00 00 00 06 从站地址(1字节) **功能码(1字节) 数据(0-252字节)
MBAP=00 00 00 00 00 06
PDU=功能码(1字节) 数据(0-252字节)

   

4. Modbus 是半双工通讯，采用主从通讯方式，同一时刻只能有一个主站存在。 


5.一、单播(unicast)：

      主机之间“一对一”的通讯模式，网络中的交换机和路由器对数据只进行转发不进行复制。如果10个客户机需要相同的数据，则服务器需要逐一传送，
	  重复10次相同的工作。但由于其能够针对每个客户的及时响应，所以现在的网页浏览全部都是采用IP单播协议。
	  网络中的路由器和交换机根据其目标地址选择传输路径，将IP单播数据传送到其指定的目的地。
单播的优点：

1.  服务器及时响应客户机的请求

2.  服务器针对每个客户不通的请求发送不通的数据，容易实现个性化服务。

单播的缺点：      

1. 服务器针对每个客户机发送数据流，服务器流量＝客户机数量×客户机流量；在客户数量大、每个客户机流量大的流媒体应用中服务器不堪重负。

二、 广播(broadcast)：

      主机之间“一对所有”的通讯模式，网络对其中每一台主机发出的信号都进行无条件复制并转发，
	  所有主机都可以接收到所有信息（不管你是否需要），由于其不用路径选择，所以其网络成本可以很低廉。
	  
	  广播的优点：

1. 网络设备简单，维护简单，布网成本低廉

2. 由于服务器不用向每个客户机单独发送数据，所以服务器流量负载极低。

广播的缺点：

1.无法针对每个客户的要求和时间及时提供个性化服务。

- 单播：主设备仅寻址单个从设备，从设备接收并处理请求后，向主设备返回一个响应报文，即应答。
这种模式下，一个Modbus事务处理包含两个报文：一个是主设备的请求报文，一个是从设备的响应报文。
- 广播：主设备向所有的从设备发送请求指令，而从设备接收广播指令后，仅需要进行相关指令的事务处理而不需要返回应答，
下·基于此，广播模式下，请求指令必须是Modbus标准功能中的写指令。